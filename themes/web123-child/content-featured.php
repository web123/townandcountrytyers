<?php 

  $custom_query_args = array(
  'post_type'  => 'prod',
  'meta_key'   => '_is_ns_featured_post',
  'meta_value' => 'yes',
  );
  $custom_query = new WP_Query( $custom_query_args );
?>
<div class="i001-product-list-thumbnails featuredd">
	<div class="vc_row wpb_row section vc_row-fluid ">
<?php 
while($custom_query->have_posts()):  $custom_query->the_post(); 

?>
<div class="wpb_column vc_column_container vc_col-sm-3" >
    <div class="vc_column-inner">
               <div style="text-align:center;min-height:194px;"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?>
                  </a>
               </div>
               <div class="i001-product-list-details">
                  <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
               </div>
               <div class="i001-product-list-buttons">
                  <a type="button" href="<?php the_permalink(); ?>" class="i001-css-button new_v01">View Details</a>  
               </div>
            </div>
            </div>
<?php 

	endwhile;
	?>
	</div>
	</div>
	
 