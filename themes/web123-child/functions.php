<?php
/**
 *	Web123 WordPress Theme
 *
 */

// This will enqueue style.css of child theme

function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_style( 'web123-custom', get_stylesheet_directory_uri() . '/custom.css' );
//	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );



function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }
				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}
				body {background-color: #000!important; }
    </style>';
}

add_action('login_head', 'custom_login_logo');


/* Hide WP version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
function remove_wp_version_strings( $src ) {
     global $wp_version;
     parse_str(parse_url($src, PHP_URL_QUERY), $query);
     if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
          $src = remove_query_arg('ver', $src);
     }
     return $src;
}
add_filter( 'script_loader_src', 'remove_wp_version_strings' );
add_filter( 'style_loader_src', 'remove_wp_version_strings' );

/* Hide WP version strings from generator meta tag */
function wpmudev_remove_version() {
return '';
}
add_filter('the_generator', 'wpmudev_remove_version');


//custome post type for Product start

register_post_type( 'Prod',

    array(

        'labels' => array(

                'name' => __( 'Products' ),

                'singular_name' => __( 'Products' ),

                'add_new' => __( 'Add Products' ),

                'add_new_item' => __( 'Add New Products' ),

                'edit' => __( 'Edit' ),

                'edit_item' => __( 'Edit Products' ),

                'new_item' => __( 'New Products' ),

                'view' => __( 'View Products' ),

                'view_item' => __( 'View Products' ),

                'search_items' => __( 'Search Products' ),

                'not_found' => __( 'No Products' ),

                'not_found_in_trash' => __( 'No Products found in Trash' ),

                'parent' => __( 'Parent Products' ),



        ),

        'public' => true,

        'has_archive' => true,



        'show_ui' => true,

        'hierarchical' => true,

        'query_var' => true,

        'rewrite'      => array( 'slug' => 'Products', 'with_front' => false ),

        'supports' => array('title', 'editor', 'author','custom-fields','page-attributes','thumbnail','excerpt','comments'),



    )

);



function add_custom_taxonomies() {

  // Add new "Locations" taxonomy to Posts

  register_taxonomy('prod_cat', 'prod', array(

    // Hierarchical taxonomy (like categories)

    'hierarchical' => true,

    // This array of options controls the labels displayed in the WordPress Admin UI

    'labels' => array(

      'name' => _x( 'Product Category', 'taxonomy general name' ),

      'singular_name' => _x( 'Product Category', 'taxonomy singular name' ),

      'search_items' =>  __( 'Search Product Category' ),

      'all_items' => __( 'All Product Category' ),

      'parent_item' => __( 'Parent Product Category' ),

      'parent_item_colon' => __( 'Parent Product Category:' ),

      'edit_item' => __( 'Edit Product Category' ),

      'update_item' => __( 'Update Product Category' ),

      'add_new_item' => __( 'Add New Product Category' ),

      'new_item_name' => __( 'New Product Category Name' ),

      'menu_name' => __( 'Product Category' ),

    ),

    // Control the slugs used for this taxonomy

    'rewrite' => array(

      'slug' => 'products', // This controls the base slug that will display before each term

      'with_front' => false, // Don't display the category base before "/locations/"

      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"

    ),

  ));

}

add_action( 'init', 'add_custom_taxonomies', 0 );



//custome post type for Product End


function show_cat(){

ob_start();
$custom_terms = get_terms( 'prod_cat', array( 'parent' => 0,'hide_empty' => 0 ) );
//$custom_terms = get_terms('prod_cat');

?>

<div class="i001-catlist cat">

  <?php


foreach($custom_terms as $custom_term) {

        $term_link=get_term_link($custom_term);

        ?>
<div style="overflow: hidden; padding: 16px 0 18px;border-top: 1px dashed #e5e5e5;">
        

            <h4><a style="color: #000000;
    text-decoration: none;"href="<?php echo  esc_url( $term_link ); ?>"> <?php echo $custom_term->name ; ?></a></h4>
            <p></p>
            <p>
                <a style="display: inline-block !important;
    color: #ffffff;
    text-decoration: none;
    text-align: center;
    float: none;
    background: #cdd2d6;
    border-radius: 0px;
    padding: 8px 38px;
    margin-top: 6px;
    margin-bottom: 20px;" href="<?php echo  esc_url( $term_link ); ?>" class="i001-css-button new_v01">Read More</a></p>
        

    </div>
        
<?php  

} ?>

</div>

<?php

$cont=ob_get_contents();

ob_end_clean();

return $cont;
}





add_shortcode( 'show_prod_cat', 'show_cat' );




add_filter( 'template_include', 'include_template_function3', 1 );



function include_template_function3( $template_path ) {

    if ( get_post_type() == 'prod' ) {

        if ( is_single() ) {

            // checks if the file exists in the theme first,

            // otherwise serve the file from the plugin

            if ( $theme_file = locate_template( array ( 'product_single.php' ) ) ) {

                $template_path = $theme_file;

            } else {

                $template_path = get_template_directory() . '/product_single.php';

            }

        }

    }

    return $template_path;

}




// Unset URL from comment form

function crunchify_move_comment_form_below( $fields ) { 

    $comment_field = $fields['comment']; 

    unset( $fields['comment'] ); 

    unset( $fields['url'] ); 

    

    $fields['comment'] = $comment_field;



    return $fields; 

} 

add_filter( 'comment_form_fields', 'crunchify_move_comment_form_below' );



// Add placeholder for Name and Email

function modify_comment_form_fields($fields){

   

$fields['Company Name'] = '<p class="comment-form-url">' .

   '<label for="url" class="label1" >' . __( 'Company Name:', 'domainreference' ) . '</label>' .

       '<input id="compnayname" name="Company Name" placeholder="" type="text" value="' . esc_attr( $commenter['comment_author_cname'] ) . '" size="30" /> ' .

      

             '</p>';

   $fields['phone No.'] = '<p class="comment-form-url">' .

   '<label for="url" class="label1">' . __( 'Phone Number:', 'domainreference' ) . '</label>' .

       '<input id="Phno1" name="phno" placeholder="" type="text" value="' . esc_attr( $commenter['comment_author_phno'] ) . '" size="30" /> ' .

      

             '</p>';


            


    return $fields;



}

add_filter('comment_form_default_fields','modify_comment_form_fields');




function wpsites_change_comment_form_submit_label($arg) {
$arg['label_submit'] = 'Make Enquiry';
return $arg;
}

add_shortcode('featured_prod',function(){

get_template_part('content','featured');

});



function comment_form_submit_button($button) {
$button =
'<button class="submit-btn" type="submit">Make Enquiry<i class="icon-paper-plane"></i></button>' . //Add your html codes here
get_comment_id_fields();
return $button;
}
add_filter('comment_form_submit_button', 'comment_form_submit_button');


function pippin_add_taxonomy_filters() {
  global $typenow;
  
  // an array of all the taxonomyies you want to display. Use the taxonomy name or slug
  $taxonomies = array('location');
  
  // must set this to the post type you want the filter(s) displayed on
  if( $typenow == 'wptpost' ){
    
    foreach ($taxonomies as $tax_slug) {
      $tax_obj = get_taxonomy($tax_slug);
      $tax_name = $tax_obj->labels->name;
      $terms = get_terms($tax_slug);
      if(count($terms) > 0) {
        echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
        echo "<option value=''>Show All $tax_name</option>";
        foreach ($terms as $term) { 
          echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
        }
        echo "</select>";
      }
    }
  }
}
add_action( 'restrict_manage_posts', 'pippin_add_taxonomy_filters' );



function site_base_url(){
$base_url = get_site_url();
return $base_url;
}
add_shortcode('BU','site_base_url');