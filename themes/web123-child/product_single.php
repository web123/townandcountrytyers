<?php
   /**
    * Template Name:Custom Blog Single Page
    *
    */
   get_header();
    global $post;
   $mypost = array('post_type' => 'prod',);
   $loop = new WP_Query($mypost);
   //echo "<pre>"; print_r($post);
   ?>
<div class="container" style="padding-top:30px;padding-bottom:30px;">
    <div class="container_inner default_template_holder clearfix page_container_inner">
        <div class="vc_row wpb_row section vc_row-fluid" style=" text-align:left;">
            <div class=" full_section_inner clearfix">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <div style="margin-bottom:0px;" class="breadcrumbs crumbs" typeof="BreadcrumbList" >
                                       <?php if(function_exists('bcn_display'))
                                          {
                                              bcn_display();
                                          }?>
                                    </div>
                                    <h1 style="display:none;"><?php the_title(); ?></h1>
                                    <input type="hidden" id="pid" value="<?php the_title(); ?>" />
                                    <input type="hidden" id="ppid" value="<?php the_ID(); ?>" />
                                    <input type="hidden" id="plid" value="<?php the_permalink(); ?>" />
                                    <br>
                                    <div class="cont">
                                       <div>
                                          <?php
                                             $content = $post->post_content;
                                             $content = preg_replace("/<img[^>]+\>/i", " ", $content);
                                             $content = apply_filters('the_content', $content);
                                             $content = $content;
                                             echo  $content ; ?>

                                       </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
         <div class="vc_row wpb_row section vc_row-fluid" >
            <div class=" full_section_inner clearfix">
                <div class="wpb_column vc_column_container vc_col-sm-9">
                    <div class="vc_column-inner ">
                    <?php $orig_post = $post;
global $post;
//echo $post->ID.'HELLO';
$categories = get_the_terms($post->ID,'prod_cat');
//print_r($categories);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category)
//print_r($individual_category);
$category_ids[] = $individual_category->term_id;
$args=array(
    'post_type'        => 'prod',
'tax_query' => array(
        array(
            'taxonomy' => 'prod_cat',
            'field'    => 'term_id',
            'terms'    => $category_ids,
        ),
    ),
'post__not_in' => array($post->ID),
'posts_per_page'=> 3 // Number of related posts that will be shown.

);

$my_query = new WP_Query($args);
//print_r($my_query);
if( $my_query->have_posts() ) {

echo '<div id="related_posts" class="mpad20" style="font-size:16px;"><h3>You might also like...</h3><div class="vc_row" style=" text-align:center;">
 ';
while( $my_query->have_posts() ) {
$my_query->the_post();?>
<div class="vc_col-sm-4" style="margin-top:20px;">
<div class="i001-product-list-item f">
            <div class="i001-product-list-image"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?>
               </a>
            </div>
            <div class="i001-product-list-details" style="background: #f5f5f5;">
               <h4 style="min-height:80px;"><a href="<?php echo the_permalink(); ?>"> <?php the_title(); ?> </a> </h4>
            </div>
            <div class="i001-product-list-buttons" style="background: #f5f5f5;">
               <a type="button" href="<?php echo the_permalink(); ?>" class="i001-css-button new_v01">View Details</a>

            </div>
         </div>
</div>

<?php
}
echo '</div></div>';
}
}
$post = $orig_post;
wp_reset_query(); ?>
                                    </div> </div>
                                    </div>
                                    </div>

    </div>
</div>
  <?php //wp_reset_query(); ?>


<script>
   (function($) {
   $( document ).ready(function() {
   	$( "#clickme" ).click(function() {
   	$( ".form123" ).toggle( "slow");
   });
   });}(jQuery));

</script>
<?php get_footer(); ?>
