<?php
/**
* Template Name: EnquiryForm
*
*/
get_header(); 
global $post;
$product = get_post($_POST['ppid']);
//echo '<pre>';
//print_r($product);
//echo '</pre>';
?>
<div class="container" style="padding-top:30px;">
    <div class="container_inner default_template_holder clearfix page_container_inner">
        <input type="hidden" id="product_link" value="<?php echo $_POST['plid']; ?>">
        <input type="hidden" id="product_name" value="<?php echo $_POST['pid']; ?>">
        <p><?php echo do_shortcode($product->post_content); ?></p>
        
        <div class="prod_details" style="display:none;">
            <div class="vc_row wpb_row section vc_row-fluid" style=" text-align:left;">
               <div class="wpb_column vc_column_container vc_col-sm-6"> 
            <h3><?php echo $product->post_title; ?></h3>
            <p><?php echo do_shortcode($product->post_content); ?></p>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-6">
                Image
                </div>
            </div>
        </div>
        
        <div class="form123" style="padding:30px;">
        <?php the_content(); ?>
        </div>
        </div>
        </div>
        
<?php get_footer(); ?>
<script>
(function($) {
   $( document ).ready(function() {
   	var pro_name = $('#product_name').val();
       var pro_link = $('#product_link').val();
       console.log(pro_name);
       $('.product-name').val(pro_name);
       $('.product-link').val(pro_link);
       
   });}(jQuery));
        
        
       </script>