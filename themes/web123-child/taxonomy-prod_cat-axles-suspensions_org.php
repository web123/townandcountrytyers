<?php
get_header();
//echo 'rakeshggg';
global $wp_query;
$cat_id = $wp_query->get_queried_object_id();
$img_banner = get_field('banner_image', 'category_'.$cat_id); 
$img_category = get_field('category_image', 'category_'.$cat_id);
$img_sidebar = get_field('category_sidebar_image', 'category_'.$cat_id);
$img_second_sidebar = get_field('category_second_sidebar_image', 'category_'.$cat_id);
$taxonomy = 'prod_cat';
?>
<div class="title_outer title_without_animation with_image" data-height="186">
      <div class="title title_size_small  position_left " style="height:186px;">
         <div class="image responsive"><img itemprop="image" src="<?php  echo $img_banner['url']; ?>" alt="&nbsp;"> </div>
                                 </div>
         </div>
<div class="container" style="padding-top:30px;">
                     <div class="container_inner default_template_holder clearfix page_container_inner">
                         <div class="vc_row wpb_row section vc_row-fluid" style=" text-align:left;">
                             <div class=" full_section_inner clearfix">
                             <div class="wpb_column vc_column_container vc_col-sm-9">
                                 <div class="vc_column-inner ">
                                     <div class="wpb_wrapper">
                                          <div class="wpb_text_column wpb_content_element ">
                                             <div class="wpb_wrapper">
                                                
                                                   <?php  $term = get_term( $cat_id, $taxonomy ); ?>
                                                   <div class="vc_row" data-id="<?php echo  $cat_slug ?>">
                                                       <?php if(empty($img_category['url'])) { ?>
                                                      <div class="vc_col-sm-12" data-id="<?php echo  $cat_slug ?>">
                                                         <!-- <h1 class="cattitle" data-id="<?php echo  $cat_slug ?>"><?php echo $term->name; ?></h1> -->
                                                         <p><?php echo do_shortcode($term->description); ?></p>
                                                      </div>
                                                      <?php } else  { ?>
                                                          <div class="vc_col-sm-7" data-id="<?php echo  $cat_slug ?>">
                                                         <!-- <h1 class="cattitle" data-id="<?php echo  $cat_slug ?>"><?php echo $term->name; ?></h1> -->
                                                         <p><?php echo do_shortcode($term->description); ?></p>
                                                      </div>
                                                      <div class="vc_col-sm-5 extra-category">
                                                         <img  class="imgaaa11" src="<?php  echo $img_category['url']; ?>" alt="">
                                                      </div>
                                                     <?php } ?>
                                                      
                                                   </div><!-- END disimg -->
                                                   <?php

                                                      $args=array( 'taxonomy' => 'prod_cat','parent'=>$cat_id,'hide_empty'=>false,'order' => 'DSC');

                                                      $child_terms=get_terms($args);
                                                      //parent
                                                      foreach ($child_terms as $child) {
                                                   //print_r($child);
                                                         ?>
                                                         
                                                         <div class="vc_row" data-id="<?php echo  $cat_slug ?>" style="margin: 15px 0px;">
                                                       
                                                      <div class="vc_col-sm-12" data-id="<?php echo  $cat_slug ?>">
                                                         <h3 class="cattitle" style="font-weight:bold;" data-id="<?php echo  $cat_slug ?>"><?php echo $child->name; ?></h3>
                                                         <?php if($child->term_id==44){
                                                         ?>
                                                         <div><p>Town and Country Tyres is a long-standing supplier of high quality suspensions to the agricultural and original 
                                                         equipment markets of Australia. We carry the full range of ADR agricultural suspensions which give you the durability 
                                                         you expect and deserve.</p>
                                    
                                                
                                                         <div class="cms-contentimage-right">
                                                             <img class="alignright" src="<?php echo do_shortcode('[BU]'); ?>/wp-content/uploads/2017/10/1563444.jpg" alt="">
                                                             </div><br>
                                                             We already supply a number of Australia’s key machinery manufacturers, but we will alwayshave time to help you choose 
                                                             the axle and suspension system that is right for you.&nbsp;<br><p></p><p style="margin-top: 10px;">For more information on our Mechanical Bogie Suspensions 
                                                             and&nbsp;Tandem &amp; Tridem Mechanical Suspensions click below.</p></div>
                                                              <h3 style="margin-bottom:12px;"><br> 
                                                             <?php echo '<a class="i001-css-button" style="padding:0px 25px;" href="' . get_term_link( $child->term_id, $taxonomy_name ) . ' " >Read More 
                                                                </a>' ?></h3>
                                                             
                                                         <?php 
                                                         } else if($child->term_id==45) { ?>
                                                         <div style="margin-top:15px;"><p><img class="alignright" src="<?php echo do_shortcode('[BU]'); ?>/wp-content/uploads/2017/10/1563588.jpg" alt=""> The professionalism 
                                                         that marks the ADR products is also found in its range of spare
                                                         parts and accessories. To find out more click below.
                                                         </p>
                                                         <p></p>
                                                         </div>
                                                          <h3 style="margin-bottom:12px;"><br> 
                                                         <?php echo '<a class="i001-css-button" style="padding:0px 25px;" href="' . get_term_link( $child->term_id, $taxonomy_name ) . ' " >Read More 
                                                                </a>' ?></h3>
                                                                <div style="height:50px;"></div>
                                                                <div>For further information on Axles and Suspension please 
                                                                contact the friendly team at Town &amp; Country Tyres
                                                                on&nbsp;<b>02 6953 7711&nbsp;</b>or&nbsp;<a href="mailto:sales@townandcountrytyres.com.au">email us</a>!<br></div>
                                                         <?php } else { ?>
                                                         <p><?php echo do_shortcode($child->description); ?> </p>
                                                         <?php } ?>
                                                      </div>
                                                      
                                                      
                                                   </div><!-- END disimg 2-->
                                                          
                                                         <?php
                                                         //echo  $child->name.'<br>';

                                                            // child of parent foreach 
                                                            $child_terms2=get_terms(array( 'taxonomy' => 'prod_cat','parent'=>$child->term_id,'hide_empty'=>false));
                                                         foreach ($child_terms2 as $child2) {
                                                            //print_r($child2);
                                                            $term_id = $child2->term_id;
                                                            $img_category = get_field('category_image', 'category_'.$term_id);

                                                            //echo '--'.$child2->name.'<br>';?>
                                                            <div class="vc_row">
                                                                <div class="vc_col-sm-9">
                                                                    <h5> <?php echo '<a href="' . get_term_link( $child2, $taxonomy_name ) . '">' . $child2->name . '</a>'  ?></h5>
                                                                    <p id="text-padding"> <?php echo wp_trim_words($child2->description, 40, '...' ); ?></p>
                                                                    <h3 style="margin-bottom:12px;"><br> 
                                                               <?php echo '<a class="i001-css-button" style="padding:0px 25px;" href="' . get_term_link( $child2, $taxonomy_name ) . ' " >Read More 
                                                                </a>' ?><br></h3>
                                                                    </div>
                                                                <div class="vc_col-sm-3"><img  class="aligncenter" src="<?php  echo $img_category['url']; ?>" alt=""></div>
                                                            </div>
                                                           

                                                         <?php }
                                                            if(empty($child_terms2))
                                                            {
                                                                //print_r($term);
                                                                if($child->term_id==44) {}
                                                                else if($child->term_id==45) {}
                                                                else {
                                                                ?>
                                                                <div class="vc_row">
                                                                    <div class="vc_col-sm-12">
                                                                        <h3 style="margin-bottom:12px;"><br> 
                                                               <?php echo '<a class="i001-css-button" style="padding:0px 25px;" href="' . get_term_link( $child->term_id, $taxonomy_name ) . ' " >Read More 
                                                                </a>' ?><br></h3>
                                                                        
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                }
                                                            }
                            
                                                         # code...
                                                       }
                                                    ?>
<div style="height:32px;"></div>
<div class="form123">
   <?php $customfield = get_field('contact_form_7', 'category_'.$cat_slug); ?> 
   <?php 
      if($customfield != ''){
      echo do_shortcode($customfield); 
      }
      ?>
      </div>
                                             </div>
                                          </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="wpb_column vc_column_container vc_col-sm-3">
                                 <div class="vc_column-inner ">
                                     <div class="wpb_wrapper">
                                           <div class="wpb_text_column wpb_content_element ">
                                             <div class="wpb_wrapper">
                                                 <div style="text-align:center">
                                                <a href="<?php echo get_site_url(); ?>/contact-us/"><img  class="imgaaa" src="<?php  echo $img_sidebar['url']; ?>" alt=""></a>
                                                <br>
                                                <a href="<?php echo get_site_url(); ?>/contact-us/"><img  class="imgaaa" src="<?php  echo $img_second_sidebar['url']; ?>" alt=""></a>
                                                </div>
                                             </div>
                                          </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
<div class="clearfix"></div>



  <?php 
get_footer();
 ?>

