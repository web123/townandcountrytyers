<?php 

?>
<style>.content {
    margin-top: 1px;
}</style>
<?php 
global $qode_options_proya;
$blog_hide_comments = "";
if (isset($qode_options_proya['blog_hide_comments'])) {
	$blog_hide_comments = $qode_options_proya['blog_hide_comments'];
}

$blog_hide_author = "";
if (isset($qode_options_proya['blog_hide_author'])) {
    $blog_hide_author = $qode_options_proya['blog_hide_author'];
}

$qode_like = "on";
if (isset($qode_options_proya['qode_like'])) {
	$qode_like = $qode_options_proya['qode_like'];
}
?>
	
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post_content_holder">
		

		
		<div class="t-std-list-item" style="width: 94%;    margin: 10px 0 0 0; padding: 10px 0 0 0; border-top: 1px dashed #ccc;">
				<h4 style="font-family: 'Lato', sans-serif;">

					<a style="    color: #023e7f;text-decoration: none; " href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
				<p style="font-size: 14px;color: #4c4d4f; font-family: 'Lato', sans-serif;padding-top: 21px!important; padding-bottom: 17px!important;"><?php 
$content = $post->post_content;
$content = preg_replace("/<img[^>]+\>/i", " ", $content);          
$content = apply_filters('the_content', $content);
$content = str_replace(']]>', ']]>', $content);
echo wp_trim_words( $content, 40, '...' ); ?></p>
				<div class="t-std-list-sch-scr">
					 <!-- Score: 154 --> | <a href="<?php the_permalink(); ?>"><?php _e('Read More','qode'); ?></a>
				</div>
		</div>


	</div>
</article>



		<style>
		.t-std-list .t-std-list-item {
    margin: 10px 0 0 0;
    padding: 10px 0 0 0;
    border-top: 1px dashed #ccc;
}
.t-std-list-sch-scr {
    background: #eee;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    text-align: right;
    padding: 7px;
    font-size: 7pt;
    color: #888;
}

.title.title_size_small.position_left {
    height: auto!important;
}

.title_outer.title_without_animation {
    height: auto!important;
}


.title_holder {
    padding-top: 47px!important;
}


.blog_holder.blog_large_image {
   display: -webkit-box;
   width: 70%;
}
.alldiv {
   margin-left: 50px;
}
		</style>