<?php get_header(); ?>
<?php 

global $wp_query;
   
   
 
   $cat_slug = $wp_query->get_queried_object_id();
   $img_banner = get_field('banner_image', 'category_'.$cat_slug); 
   $img_category = get_field('category_image', 'category_'.$cat_slug);
   $img_sidebar = get_field('category_sidebar_image', 'category_'.$cat_slug);
   $img_second_sidebar = get_field('category_second_sidebar_image', 'category_'.$cat_slug);
    $taxonomy = 'prod_cat';
    $term = get_term( $cat_slug, $taxonomy );

   
   
   ?>
<?php if(!empty($img_banner['url'])): ?>
<div class="title_outer title_without_animation with_image" data-height="186">
      <div class="title title_size_small  position_left " style="height:186px;">
         <div class="image responsive"><img itemprop="image" src="<?php  echo $img_banner['url']; ?>" alt="&nbsp;"> </div>
                                 </div>
         </div>
         <?php endif; ?>
<div class="container">
                     <div class="container_inner default_template_holder clearfix page_container_inner">
                         <div class="vc_row wpb_row section vc_row-fluid mpad20">
                             <div class=" full_section_inner clearfix">
                             <div class="wpb_column vc_column_container vc_col-sm-9">
                                 <div class="vc_column-inner ">
                                     <div class="wpb_wrapper">
                                           <div class="wpb_text_column wpb_content_element ">
                                          <div class="wpb_wrapper">
                                              <div class="bread" typeof="BreadcrumbList" style="margin-bottom:20px;" >
                                                   <?php if(function_exists('bcn_display'))
                                                      {
                                                          bcn_display();
                                                      }?>
                                                </div>
                                                <!--MYCODE-->
                                                <?php if(!empty($term->description)): ?>
                                                 <div class="vc_row" data-id="<?php echo  $cat_slug ?>">
                             <?php if(!empty($img_category['url'])) : ?>                        
         <div class="vc_col-sm-12" style="padding-left: 0px !important; padding-right: 0px !important;">
             <?php else: ?>
                                    <div class="vc_col-sm-12">
                                <?php endif; ?>
            <h5 class="cattitle" data-id="<?php echo  $cat_slug ?>"><?php echo $term->name; ?></h5>
            <p><?php echo do_shortcode($term->description); ?> </p>
         </div>
         <!--<div class="vc_col-sm-5">
            <img  class="" src="<?php  echo $img_category['url']; ?>" alt="">
         </div>-->
         </div>
         <?php else: ?>
         <div class="vc_row" data-id="<?php echo  $cat_slug ?>">
         <div class="vc_col-sm-12">
             <h3><?php echo $term->name; ?></h3>
             </div>
             </div>
         <?php endif; ?>
         <?php if(!empty($term->description)): ?>
         
         <?php endif; ?>
     
       <?php
      $term_id = $cat_slug;
      //echo $term_id;
      
      $taxonomy_name = 'prod_cat';     
      $termchildren = get_terms( array( 
    'taxonomy' => $taxonomy_name,
    'parent'   => $term_id,
    'hide_empty' => false
     ) );
      //print_r($termchildren);
      $number = sizeof ($termchildren);
      
      ?>
  
   <?php 
      if($number > 0){ ?>
      <style>
           .cattitle {display:none;}
       </style>
      <div class="vcbg">
      <?php
      $cnt=0;
      foreach ( $termchildren as $children ) {
         $child=$children->term_id;
          $img_category = get_field('category_image', 'category_'.$child);
          $term = get_term_by( 'id', $child, $taxonomy_name );
         ?>
 
                                                
         <div class="vc_row wpb_row section vc_row-fluid" style=" text-align:left;">
                             <div class=" full_section_inner clearfix">
                                 <?php if(!empty($term->description)): ?>
                                 <?php if(!empty($img_category['url'])) : ?>
                                    <div class="vc_col-sm-7">
                                       
                                <?php else: ?>
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                <?php endif; ?>
                                              
                                                 <h5> <?php echo $term->name;  ?></h5>
        <p> <?php echo wp_trim_words($term->description, 40, '...' ); ?></p>
        
        
        
            <h3 style="margin-bottom:12px;"><br> <?php echo '<a style="padding:5px 35px;" class="i001-css-button" href="' . get_term_link( $child, $taxonomy_name ) . ' " >'.$term->name.'</a>' ?><br></h3>
            
            
                                            </div>
        
<div class="vc_col-sm-5">
                                
                                                <img  class="imgaaa11" src="<?php  echo $img_category['url']; ?>" alt="">
                                            </div>
                                            
                                            <?php else : ?>
                                            <div class="vc_col-sm-12">
                                                
                                                <h5 class="wodesc"> 
                                                <?php echo '<a style="font-size:15px;font-weight:400;" href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a>'  ?>
                                                </h5>
                                            
                                            </div>
                                            <?php endif; ?>
                                            </div></div>
                                            <?php if(!empty($term->description)): ?>
                                            <div style="height:20px;"></div>
                                             <?php endif; ?>
            
                                            
                                            
                                            
                                            
                                            
      
        


         
      
   
   <?php }?>
   </div>
   <?php } else{
       ?>
       <style>
           .cattitle {display:block;}
       </style>
       <?php
      $cat_slug = $wp_query->get_queried_object_id();
          
       //$cat_slug =  $attr['id'];     
            
       //print_r($cat_slug); ?>
  <div class="vc_row wpb_row section " style=" text-align:center;">
                             <div class=" full_section_inner clearfix">
         <?php
            
            
                 $args = array('post_type' => 'prod', 'posts_per_page' => -1, 'order' => 'ASC','post_parent' => 0 ,'tax_query' => array( array( 'taxonomy' => 'prod_cat', 'field' => 'id', 'terms' => $cat_slug ) ) );
            
                        
            
                        $mypost = new WP_Query($args);         
                        
                         global $post;             
                         $posts = $mypost->get_posts();   
                         
                         foreach ($posts as $post11) {        
            
              
            
                             ?>
                           <div class="wpb_column vc_column_container vc_col-sm-4" style="margin-top:20px;">
                                 <div class="vc_column-inner ">
                                     <div class="wpb_wrapper">
                                           <div class="wpb_text_column wpb_content_element ">
                                          <div class="wpb_wrapper">

         <div class="i001-product-list-item f">
            <div class="i001-product-list-image"><a href="<?php echo get_post_permalink($post11->ID); ?>"><img src="<?php echo get_the_post_thumbnail_url($post11->ID, 'full') ?>" alt="">
               </a>
            </div>
            <div class="i001-product-list-details" style="background: #f5f5f5;">
               <h4 style=min-height:80px;"><a href="<?php echo get_post_permalink($post11->ID); ?>"><?php echo $post11->post_title ?></a></h4>
            </div>
            <div class="i001-product-list-buttons" style="background: #f5f5f5;">
               <?php echo '<a type="button" href="' . get_post_permalink($post11->ID) . '" class="i001-css-button new_v01">View Details</a>'; ?>
             
            </div>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>
       
         <?php     } ?>
         
         </div>
   </div>
   
        <?php  } ?>
         <!-- show slider below  only in category 5 -->
         
         <div class="i001-detail i001-image-right i001-image-med" id="i001-329891">
            <h4><b><?php echo get_field('wpt_slider_heading', 'category_'.$cat_slug); ?></b></h4>
         </div>
        <?php $customslider = get_field('wpt_slider', 'category_'.$cat_slug); ?> 
 
           <?php  echo do_shortcode($customslider);  ?>
         <div class="i001-detail i001-image-right i001-image-med" id="i001-329562">
            <div><br></div>
            <?php echo get_field('wpt_slider_content', 'category_'.$cat_slug); ?>
         </div>          
      
      

<div class="clearing"></div>
<div class="form123">
   <?php $customfield = get_field('contact_form_7', 'category_'.$cat_slug); ?> 
   <?php 
      if($customfield != ''){
      echo do_shortcode($customfield); 
      }
      ?>
      </div>
                                                <!--END MY CODE-->
                                              
                                              </div><!--wpb_wrapper-->
                                              </div><!--wpb_wrapper-->
                                              </div><!--wpb_text_column wpb_content_element-->
                                              </div><!--wpb_wrapper-->
                                              </div><!--vc_col-sm-8-->
                              <div class="wpb_column vc_column_container vc_col-sm-3">
                                 <div class="vc_column-inner ">
                                     <div class="wpb_wrapper">
                                           <div class="wpb_text_column wpb_content_element ">
                                          <div class="wpb_wrapper">
                                              <div style="text-align:center">
                                              <a href="<?php echo get_site_url(); ?>/contact-us/"><img  class="imgaaa" src="<?php  echo $img_sidebar['url']; ?>" alt=""></a>
                                               <a href="<?php echo get_site_url(); ?>/contact-us/"><img  class="imgaaa" src="<?php  echo $img_second_sidebar['url']; ?>" alt=""></a>
                                               </div>
                                              </div><!--wpb_wrapper-->
                                              </div><!--wpb_wrapper-->
                                              </div><!--wpb_text_column wpb_content_element-->
                                              </div><!--wpb_wrapper-->
                                              </div><!--vc_col-sm-8-->
                                              </div><!--full_section_inner-->
                                              </div><!--vc_row-->
                                              
          
          
          
                         </div>
                         </div><!--container end-->
<div style="height:32px;"></div>
<?php get_footer(); ?>